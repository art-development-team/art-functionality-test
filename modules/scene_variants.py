#!/usr/bin/env python3

"""
For a given scene, gives the variation of that scene based on defines
:return: A list of various scenes to render describe by a tuple.
    - The first item is a descriptive scene that can be append to the output
    - The second item is a list of arguments (defines) to give to artist
"""
def get_scene_variants(scene):
    return {
        'TwoSphereScene.arm' : two_sphere_scene_variants(),
        'PolarisingScene.arm': polarising_scene_variants(),
        'SimpleScene.arm'    : simple_scene_variants(),
        'Material.arm'       : material_scene_variants(),
        'GlowingObjects.arm' : glowing_objects_scene_variants(),
        'Volume.arm'         : volume_scene_variants(),
        }.get(scene, [('', [])])

def scene_expected_exit_code_for_flags(scene, flags):
    return {
        'Material.arm'  : material_expected_exit_code_flags(flags),
        }.get(scene, 0)

"""
Various scenes for 'TwoSphereScene.arm'
"""
def two_sphere_scene_variants():
    return [('', []), ('_BalanceHeuristic', ['-DALG_MIS'])]

def glowing_objects_scene_variants():
    return [
        ('_mis', ['-DALG_MIS']),
        ('_ls' , ['-DALG_LS']),
        ('_ds' , ['-DALG_DS'])
    ]

"""
Various scenes for 'PolarisingScene.arm'
"""
def polarising_scene_variants():
    polarising_scene_env_args = [
        ('blackglassfloor', ''),
        ('mixedfloor'     , 'MIXED_FLOOR'),
        ]
        
    polarising_scene_mat_args = [
        ('blackglasssphere', 'BLACK_GLASS'),
        ('glassTSsphere'   , 'GLASS_TS'),
        #('metallicTSsphere', 'METALLIC_TS'),
        ]

    # Build argument list
    args = [ [ env_define, mat_define ]
             for (env_name, env_define) in polarising_scene_env_args
             for (mat_name, mat_define) in polarising_scene_mat_args
    ]

    # Set the flags
    args = [
         [ "-D" + env if env != '' else env,
           "-D" + mat if mat != '' else mat ]
         for env, mat in args
        ]

    # Build name suffix
    suffix = [ str('_' + 'lsspt' + '_' + env_name + '_' + mat_name)
               for (env_name, env_define) in polarising_scene_env_args
               for (mat_name, mat_define) in polarising_scene_mat_args
    ]

    retVal = [ (s, a) for s, a in zip(suffix, args) ]

    return retVal

def material_scene_variants():
    material_scene_mat_args = [
        ('lambert'                , 'MATERIAL_LAMBERT'),
        ('phong'                  , 'MATERIAL_PHONG'),
        #('oren_nayar'             , 'MATERIAL_OREN_NAYAR'),
        #('perfect_reflector'      , 'MATERIAL_PERFECT_REFLECTOR'),
        #('perfect_refractor'      , 'MATERIAL_PERFECT_REFRACTOR'),
        ('smooth_fresnel'         , 'MATERIAL_SMOOTH_FRESNEL'),
        ('smooth_fresnel_gold'    , 'MATERIAL_SMOOTH_FRESNEL_WITH'),
        ('torrance_sparrow'       , 'MATERIAL_TORRANCE_SPARROW'),
        ('torrance_sparrow_gold'  , 'MATERIAL_TORRANCE_SPARROW_BLINN_SURFACE_WITH'),
        ('layered'                , 'MATERIAL_LAYERED'),
        ('glass_fresnel'          , 'MATERIAL_GLASS_FRESNEL'),
        ('glass_torrance_sparrow' , 'MATERIAL_GLASS_TORRANCE_SPARROW'),
        # ('inert'                  , 'MATERIAL_INERT'),
        #('transparent'            , 'MATERIAL_TRANSPARENT'),
        #('htsg'                   , 'MATERIAL_HTSG'),
        #('ward'                   , 'MATERIAL_WARD'),
        ('synth_fluorescent'      , 'MATERIAL_SYNTH_FLUORESCENT'),
        ('measured_fluorescent'   , 'MATERIAL_MEASURED_FLUORESCENT'),
        ]
    
    material_scene_illu_args = [
        ('diffuselight' , 'USE_DIFFUSE_ILLUMINATION'),
        ('hosekskylight', 'USE_HOSEK_SKYLIGHT'),
        ('smalllight'   , 'SMALL_LIGHT'),
        ]

    # Build argument list
    args = [ [ mat_define, illu_define ]
             for (mat_name, mat_define) in material_scene_mat_args
             for (illu_name, illu_define) in material_scene_illu_args
    ]

    # Set the flags
    args = [
         [ "-D" + env if env != '' else env,
           "-D" + mat if mat != '' else mat ]
         for env, mat in args
        ]

    # Build name suffix
    suffix = [ str('_' + mat_name + '_' + illu_name)
               for (mat_name, mat_define) in material_scene_mat_args
               for (illu_name, illu_define) in material_scene_illu_args
    ]

    retVal = [ (s, a) for s, a in zip(suffix, args) ]

    return retVal

def material_expected_exit_code_flags(flags):
    if '-p' in flags and '-DMATERIAL_PHONG' in flags:
        return 128

    return 0

"""
Various scenes for 'SimpleScene.arm'
"""
def simple_scene_variants():
    simple_scene_env_args = [
        ('cornellbox', 'CORNELL_BOX'),
        (''          , ''),
        ('floor'     , 'SHOW_FLOOR'),
        ]

    simple_scene_mat_args = [
        ('glasstorrancesparrow_row'      , 'GLASS_TORRANCE_SPARROW_ROW'),
        ('lambert'                       , 'LAMBERT'),
        ('metallicfresnel'               , 'METALLICFRESNEL'),
        #('metallictorrancesparrow_row'   , 'METALLIC_TORRANCE_SPARROW_ROW'),
        ('glass_torrance_sparrow_3shapes', 'GLASS_TORRANCE_SPARROW_3SHAPES'),
        ]

    simple_scene_illu_args = [
        ('diffuselight' , 'USE_DIFFUSE_ILLUMINATION'),
        ('hosekskylight', 'USE_HOSEK_SKYLIGHT'),
        #('softbox'      , 'USE_SOFT_BOX'),
        ]

    simple_scene_alg_args = [
        ('lsspt', 'USE_LIGHTSOURCE_PATH_TRACER'),
        ('pt'   , ''),
        ]
    
    # Build argument list
    args = [ [ env_define, mat_define, illu_define, alg_define ]
             for (env_name, env_define)   in simple_scene_env_args
             for (mat_name, mat_define)   in simple_scene_mat_args
             for (illu_name, illu_define) in simple_scene_illu_args
             for (alg_name, alg_define)   in simple_scene_alg_args
    ]

    # Set the flags
    args = [ [ "-D" + env  if env  != '' else env,
               "-D" + mat  if mat  != '' else mat,
               "-D" + illu if illu != '' else illu,
               "-D" + alg  if alg  != '' else alg ]
             for env, mat, illu, alg in args
    ]

    # Build name suffix
    suffix = [ str(('_' + alg_name  if alg_name  != '' else '') +
                   ('_' + env_name  if env_name  != '' else '') +
                   ('_' + illu_name if illu_name != '' else '') +
                   ('_' + mat_name  if mat_name  != '' else ''))
               for (env_name, env_define)   in simple_scene_env_args
               for (mat_name, mat_define)   in simple_scene_mat_args
               for (illu_name, illu_define) in simple_scene_illu_args
               for (alg_name, alg_define)   in simple_scene_alg_args
    ]

    retVal = [ (s, a) for s, a in zip(suffix, args) ]

    return retVal


def volume_scene_variants():
    volume_scene_volume_args = [
        ('homogeneous'        , 'HOMOGENEOUS'),
        ('heterogeneous_cube' , 'FLOATING_CUBE'),
        ('heterogeneous_smoke', 'HETERO_SCALE=15')
    ]

    volume_scene_scattering_args = [
        ('non_fluo'           , 'USE_NON_FLUO=0.2'),
        ('fluo'               , '')
    ]

    volume_scene_absorption_args = [
        ('non_absorption', ''),
        ('absorbs'       , 'ABSORPTION=0.2')
    ]
        
    volume_scene_alg_args = [
        ('mis', 'ALG_MIS'),
    ]

    volume_scene_integrator_args = [
        ('exp'       , 'DISTANCE_EXP'),
        ('max_exp'   , 'DISTANCE_MAX_EXP'),
        ('fluo_aware', ''),
    ]
    
    # Build argument list
    args = [ [ vol_define, scat_define, abso_define, alg_define, inte_define ]
             for (vol_name , vol_define ) in volume_scene_volume_args
             for (scat_name, scat_define) in volume_scene_scattering_args
             for (abso_name, abso_define) in volume_scene_absorption_args
             for (alg_name , alg_define ) in volume_scene_alg_args
             for (inte_name, inte_define) in volume_scene_integrator_args
    ]

    # Set the flags
    args = [ [ "-D" + vol  if vol  != '' else vol,
               "-D" + scat if scat != '' else scat,
               "-D" + abso if abso != '' else abso,
               "-D" + alg  if alg  != '' else alg,
               "-D" + inte if inte != '' else inte]
             for vol, scat, abso, alg, inte in args
    ]
    
    # Build name suffix
    suffix = [ str(('_' + vol_name  if vol_name  != '' else '') +
                   ('_' + scat_name if scat_name != '' else '') +
                   ('_' + abso_name if abso_name != '' else '') +
                   ('_' + alg_name  if alg_name  != '' else '') +
                   ('_' + inte_name if inte_name != '' else ''))
               for (vol_name , vol_define ) in volume_scene_volume_args
               for (scat_name, scat_define) in volume_scene_scattering_args
               for (abso_name, abso_define) in volume_scene_absorption_args
               for (alg_name , alg_define ) in volume_scene_alg_args
               for (inte_name, inte_define) in volume_scene_integrator_args
    ]

    retVal = [ (s, a) for s, a in zip(suffix, args) ]

    return retVal
