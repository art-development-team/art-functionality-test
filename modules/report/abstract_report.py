#!/usr/bin/env python3

from abc import ABC, abstractmethod

class AbstractReport(ABC):

    @abstractmethod
    def write(job_results, report_dir):
        pass
