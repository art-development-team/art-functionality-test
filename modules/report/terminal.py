#!/usr/bin/env python3

from modules.report.abstract_report import AbstractReport

class colour:
    PURPLE    = '\033[95m'
    CYAN      = '\033[96m'
    DARKCYAN  = '\033[36m'
    BLUE      = '\033[94m'
    GREEN     = '\033[92m'
    YELLOW    = '\033[93m'
    RED       = '\033[91m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'
    END       = '\033[0m'

class TerminalReport(AbstractReport):

    def write(job_results, report_dir):
        n_rendering_success = len(job_results['renderings_succeeded'])
        n_rendering_failure = len(job_results['renderings_failed'])

        n_compare_success = len(job_results['compares_succeeded'])
        n_compare_failure = len(job_results['compares_failed'])
        
        n_tests_success = len(job_results['tests_succeeded'])
        n_tests_failure = len(job_results['tests_failed'])
        
        time = job_results['time']

        n_tests       = n_tests_success + n_tests_failure
        n_renderings  = n_rendering_success + n_rendering_failure
        n_comparisons = n_compare_success + n_compare_failure

        percent_rendering_success = (n_rendering_success * 100) // n_renderings
        percent_compare_success   = (n_compare_success   * 100) // n_comparisons
        percent_test_success      = (n_tests_success     * 100) // n_tests

        print(2*'\n'+ colour.UNDERLINE + colour.BLUE + 'End Report:' + colour.END)
        
        colour_render = colour.GREEN if n_rendering_failure == 0 else colour.RED
        colour_diff   = colour.GREEN if n_compare_failure   == 0 else colour.RED
        colour_test   = colour.GREEN if n_tests_failure     == 0 else colour.RED

        print('\tRendering(s) done:  '
              + colour_render + str(n_rendering_success) + '/' + str(n_renderings) + '    '
              + colour.BOLD   + str(percent_rendering_success) + "%" + colour.END)
        print('\tComparison(s) done: '
              + colour_diff   + str(n_compare_success) + '/' + str(n_comparisons) + '    '
              + colour.BOLD   + str(percent_compare_success) + "%" + colour.END)
              
        print('\tTest(s) passed:     '
              + colour_test   + str(n_tests_success) + '/' + str(n_tests) + '    '
              + colour.BOLD   + str(percent_test_success) + "%" + colour.END)
        
        print('\tTotal time:     '
              + colour_test + colour.BOLD   + str(time) + " seconds" + colour.END)