#!/usr/bin/env python3

from modules.report.abstract_report import AbstractReport
from subprocess import call
from operator import attrgetter, itemgetter
import os

class HTMLReport(AbstractReport):
    def write(job_results, report_dir):
        html_head = """
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                     <title>ART Test Result</title>
                     <style type="text/css">
                         body {
                             font-family: Arial, Helvetica, sans-serif;
                             width:100%;
                             margin:auto;
                         }
                         h1 {
                             margin-top: 2em;
                             padding-bottom:0.2em;
                             border-bottom: thick solid
                         }
                         h2 {
                             margin-top: 1em;
                             padding-bottom:0.2em;
                             border-bottom: solid grey;
                             margin-left:3%;
                         }
                         h3 {
                             margin-top: 1em;
                             color:grey;
                             padding-bottom:0.2em;
                             border-bottom: thin solid grey;
                             margin-left:7%;
                         }
                         p {
                            margin-left:7%;
                         }
                         pre {
                            margin-left:7%;
                         }
                         table {
                            margin-left:7%;
                            width:93%;
                         }
                         ul {
                            margin-left:7%;
                            
                         }
                     </style>
                </head>
                <body>
        """

        html_section1_template = """
            <h1>{}</h1>
        """

        html_section2_template = """
            <h2>{}</h2>
        """

        html_section3_template = """
            <h3>{}</h3>
        """

        html_section3_label_template = """
            <h3 id="{id_a}">{title}</h3>
        """

        html_img_compare_template = """
            <table>
                <tr>
                    <th>Reference</th>
                    <th>Output</th>
                    <th>Difference</th>
                </tr>
                <tr>
                    <td align="center"><img src="{reference_path}" alt="Reference" /></td>
                    <td align="center"><img src="{render_path}" alt="Current rendering" /></td>
                    <td align="center"><img src="{difference_path}" alt="Difference" /></td>
                </tr>
            </table>
        """
        
        html_img_polarisation_template = """
            <table>
                <tr>
                    <th>dop</th>
                    <th>s0</th>
                    <th>s1</th>
                    <th>s2</th>
                    <th>s3</th>
                </tr>
                <tr>
                    <td align="center"><img src="{dop}" alt="dop" /></td>
                    <td align="center"><img src="{s0}" alt="s0" /></td>
                    <td align="center"><img src="{s1}" alt="s1" /></td>
                    <td align="center"><img src="{s2}" alt="s2" /></td>
                    <td align="center"><img src="{s3}" alt="s2" /></td>
                </tr>
            </table>
        """

        html_errors_template = """
        <h2>Errors / Difference</h2>
        <table>
            {}
        </table>
        """
        
        html_fixed_template = "<pre>{}</pre>"

        html_foot = """
            </body>
        </html>
        """

        html_ref_id_template = "<a href=\"index.html#{id_a}\">{text}</a>"

        html_plot_line_template = """
        <tr>
            <td><a href="index.html#{id_a}">{label}</a></td>
            <td>
                <div style='background-color:blue;width:{percent}%;' />
                <p>{error}</p>
            </td>
        </tr>
        """

        if not os.path.exists(report_dir):
            os.makedirs(report_dir)

        with open(os.path.join(report_dir, 'index.html'), 'w') as f:
            f.write(html_head)

            f.write(html_section1_template.format('Summary'))

            # Show ordoned list of comparisons
            test_errors = sorted(job_results['compare_metric'], key=itemgetter(1), reverse=True)
            error_html_string = ""

            # Create the body of the table and create a gnuplot file
            # gnuplot_in_file = os.path.join(report_dir, 'errors.txt')
            # gnuplot_out_file = os.path.join(report_dir, 'errors.png')
            
            # with open(gnuplot_in_file, 'w') as gnuplot_f:
            #     for s, e in test_errors:
            #         gnuplot_f.write('"' + s.replace('_', ' ') + '" '  + str(e) + '\n')
            
            
            # call(['gnuplot',
            #       '-e', 'filename_in="' + gnuplot_in_file + '";filename_out="' + gnuplot_out_file + '";',
            #       'errors.gnuplot',
            # ])
            
            #f.write('<img src="' + os.path.relpath(gnuplot_out_file, report_dir) + '" />')

            for s, e in test_errors:
                error_html_string += html_plot_line_template.format(id_a = s,
                                                                    label=s,
                                                                    percent=str((100*e)//0.5),
                                                                    error=str(e))
            
            f.write(html_errors_template.format(error_html_string))
            
            # Compute stats
            n_rendering_success = len(job_results['renderings_succeeded'])
            n_rendering_failure = len(job_results['renderings_failed'])

            n_compare_success = len(job_results['compares_succeeded'])
            n_compare_failure = len(job_results['compares_failed'])
            
            n_tests_success = len(job_results['tests_succeeded'])
            n_tests_failure = len(job_results['tests_failed'])

            n_tests       = n_tests_success + n_tests_failure
            n_renderings  = n_rendering_success + n_rendering_failure
            n_comparisons = n_compare_success + n_compare_failure
            
            time = job_results['time']

            percent_test_success      = (n_tests_success     * 100) // n_tests
            percent_test_failure      = 100 - percent_test_success

            # Success summary
            f.write(html_section2_template.format('Succeeded test(s)'))
            
            f.write('<p>Suceess rate: ' +
                    str(n_tests_success) + '/' + str(n_tests) +
                    ' [' + str(percent_test_success) + '%]</p>'
            )
            
            f.write('<ul>')
            for s in job_results['tests_succeeded']:
                f.write('<li>' + html_ref_id_template.format(id_a=s, text=s) + '</li>')
            f.write('</ul>')

            # Failure summary
            f.write(html_section2_template.format('Failed test(s)'))
            
            f.write('<p>Failure rate: ' +
                    str(n_tests_failure) + '/' + str(n_tests) +
                    ' [' + str(percent_test_failure) + '%]</p>'
            )
            
            # Time summary
            f.write(html_section2_template.format('Time'))
            
            f.write('<p>Total time: ' +
                    str(time) + ' seconds</p>'
            )
            
            f.write('<ul>')
            for s in job_results['tests_failed']:
                f.write('<li>' + html_ref_id_template.format(id_a=s, text=s) + '</li>')
            f.write('</ul>')
            
            f.write(html_section1_template.format('Results'))
            
            for k in job_results['scenes']:
                f.write(html_section2_template.format(k))

                for variant in job_results['scenes'][k]:
                    # f.write(html_section3_template.format(variant['name']))
                    f.write(html_section3_label_template.format(id_a=variant['name'], title=variant['name']))

                    arg_string = ' '.join(filter(None, variant['test_res']['render']['cmd']))
                    f.write(html_fixed_template.format(arg_string))
                    
                    renderer_exit_code = variant['test_res']['render']['return_code']

                    if renderer_exit_code == 0:
                        # Show error if available
                        if variant['test_res']['compare']['diff_possible']:
                            f.write('<p>Error: ' + str(variant['test_res']['compare']['diff_value']) + '</p>')
                        
                        reference_path_rel = ''
                        output_path_rel = ''
                        compare_path_rel = ''
                        
                        # First need to convert from TIFF to PNG for HTML support
                        with open(os.devnull,"w") as devnull_f:
                            if variant['test_res']['compare']['diff_possible']:
                                call(['convert',
                                      variant['reference_path'] + '.tiff',
                                      variant['reference_path'] + '.png'],
                                      stderr=devnull_f)

                                # Get the relative path from the html page to the image
                                reference_path_rel = os.path.relpath(variant['reference_path'],
                                                                     report_dir) + '.png'
                            
                            if variant['test_res']['render']['generated']:
                                call(['convert',
                                      variant['test_res']['render']['output_ldr'],
                                      variant['test_res']['render']['output_noext'] + '.png'],
                                      stderr=devnull_f)

                                # Get the relative path from the html page to the image
                                output_path_rel = os.path.relpath(variant['test_res']['render']['output_noext'],
                                                                  report_dir) + '.png'
                        
                            if variant['test_res']['compare']['diff_generated']:
                                call(['convert',
                                      variant['test_res']['compare']['diff_ldr'],
                                      variant['test_res']['compare']['diff_noext'] + '.png'],
                                      stderr=devnull_f)

                                # Get the relative path from the html page to the image
                                compare_path_rel = os.path.relpath(variant['test_res']['compare']['diff_noext'],
                                                                  report_dir) + '.png'
                    
                        f.write(html_img_compare_template.format(
                            reference_path  = reference_path_rel,
                            render_path     = output_path_rel,
                            difference_path = compare_path_rel
                        ))
                    
                        # Display extra results if rendering is polarised
                        if variant['test_res']['render']['polarised']:
                            with open(os.devnull,"w") as devnull_f:
                                call(['convert',
                                      variant['test_res']['render']['dop_noext'] + '.tiff',
                                      variant['test_res']['render']['dop_noext'] + '.png'],
                                      stderr=devnull_f)
                                call(['convert',
                                      variant['test_res']['render']['s0_noext'] + '.tiff',
                                      variant['test_res']['render']['s0_noext'] + '.png'],
                                      stderr=devnull_f)
                                call(['convert',
                                      variant['test_res']['render']['s1_noext'] + '.tiff',
                                      variant['test_res']['render']['s1_noext'] + '.png'],
                                      stderr=devnull_f)
                                call(['convert',
                                      variant['test_res']['render']['s2_noext'] + '.tiff',
                                      variant['test_res']['render']['s2_noext'] + '.png'],
                                      stderr=devnull_f)
                                call(['convert',
                                      variant['test_res']['render']['s3_noext'] + '.tiff',
                                      variant['test_res']['render']['s3_noext'] + '.png'],
                                      stderr=devnull_f)

                            # Get the relative path from the html page to the image
                            dop_path_rel = os.path.relpath(variant['test_res']['render']['dop_noext'], report_dir) + '.png'
                            s0_path_rel = os.path.relpath(variant['test_res']['render']['s0_noext'], report_dir) + '.png'
                            s1_path_rel = os.path.relpath(variant['test_res']['render']['s1_noext'], report_dir) + '.png'
                            s2_path_rel = os.path.relpath(variant['test_res']['render']['s2_noext'], report_dir) + '.png'
                            s3_path_rel = os.path.relpath(variant['test_res']['render']['s3_noext'], report_dir) + '.png'
                            
                            f.write(html_img_polarisation_template.format(
                                dop = dop_path_rel,
                                s0  = s0_path_rel,
                                s1  = s1_path_rel,
                                s2  = s2_path_rel,
                                s3  = s3_path_rel
                            ))
                            
                    # Display render log
                    # formated_log = ''
                    # for s in variant['test_res']['render']['log']:
                    #     formated_log += s + '<br>'
                    
                    # f.write(html_fixed_template.format(formated_log))
                    formated_log = ''
                    for s in variant['test_res']['render']['stderr']:
                        formated_log += s + '<br>'
                    
                    f.write(html_fixed_template.format(formated_log))




            f.write(html_foot)
