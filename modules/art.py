#!/usr/bin/env python3

import os, sys
from subprocess import call, Popen, PIPE

class Art:
    def version_str(major, minor, patch, devtag):
        return str(major) + '.' + str(minor) + '.' + str(patch) + str(devtag)
    
    def get_art_version(executable):
        version_string = ''

        with open(os.devnull, 'w') as err_f:
            proc = Popen([executable, '-h'], stdout=PIPE, stderr=err_f)
        
        stdout_lines = proc.stdout.readlines()
        
        if len(stdout_lines) > 2:
            version_string = stdout_lines[2].decode("utf-8")
        else:
            version_string = None
    
        return version_string
    
    def check_version_match(major, minor, patch, devtag):
        executable_to_check = [
            'arm2art',
            'artist',
            'polvis',
            'bugblatter',
            'art_imagediff',
            'tonemap'
        ]
    
        for e in executable_to_check:
            vers = Art.get_art_version(e)
            if not vers:
                print("Cannot find ART toolkit")
                return False
            if not vers.startswith(
                'ART version '
                + Art.version_str(major, minor, patch, devtag)):
                print(e)
                print(vers)
                print(Art.version_str(major, minor, patch, devtag))
                return False

        return True
    
    def arm2art(scene_file, args=[], verbose=False):
        stdout_f = open(os.devnull, 'w') if not verbose else sys.stdout
        
        arm2art_args = [ 'arm2art', scene_file, '-f' ]
        arm2art_args += filter(None, args)

        ret_val = 0
        
        ret_val = call(arm2art_args, stderr=stdout_f, stdout=stdout_f)
        
        if not verbose:
            stdout_f.close()

        return ret_val

    def artist(scene_file, output_path, samples, args=[], error_file=sys.stdout, verbose=False):
        stdout_f = open(os.devnull, 'w') if not verbose else sys.stdout
        
        artist_args = [ 'artist',
                        scene_file,
                        '-b',
                        '-o', output_path,
                        '-DSAMPLES=' + str(samples),
                        '-seed', str(1989),
                        '-j1',
        ]

        artist_args += filter(None, args)
        ret_val = 0
        
        with open(error_file, 'w') as error_f:
            ret_val = call(artist_args, stderr=error_f, stdout=stdout_f)
        
        if not verbose:
            stdout_f.close()
        
        return artist_args, ret_val

    def polvis(artraw_file, output_path, args=[], verbose=False):
        stdout_f = open(os.devnull, 'w') if not verbose else sys.stdout

        polvis_args = [ 'polvis',
                        artraw_file,
                        '-b', '-q',
                        '-o', output_path
                        ]
        
        polvis_args += filter(None, args)

        call(polvis_args, stdout=stdout_f)
        
        if not verbose:
            stdout_f.close()
        
        return polvis_args

    def bugblatter(reference_path, render_path, output_path, verbose=False):
        stdout_f = open(os.devnull, 'w') if not verbose else sys.stdout

        bugblatter_args = [ 'bugblatter',
                            render_path, reference_path,
                            '-b',
                            '-q', '-r',  '-rw', '2', '-m', '20.0',
                            '-o', output_path
        ]
        
        call(bugblatter_args, stdout=stdout_f)

        if not verbose:
            stdout_f.close()
        
        return bugblatter_args

    def art_imagediff(reference_path, render_path, output_path, verbose=False):
        stdout_f = open(os.devnull, 'w') if not verbose else sys.stdout

        art_imagediff_args = [ 'art_imagediff',
                               reference_path,
                               '-c', render_path,
                               '-o', output_path
        ]

        call(art_imagediff_args, stdout=stdout_f)
        
        if not verbose:
            stdout_f.close()
        
        return art_imagediff_args

    def tonemap(path, verbose=False):
        stdout_f = open(os.devnull, 'w') if not verbose else sys.stdout

        tonemap_args = [ 'tonemap',
                         path,
                         '-iac', '-rcsp', '-b',
        ]

        call(tonemap_args, stdout=stdout_f)

        if not verbose:
            stdout_f.close()
        
        return tonemap_args
