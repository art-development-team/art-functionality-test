#!/usr/bin/env python3

import os, sys, shutil
from subprocess import call
#from joblib import Parallel, delayed
#import multiprocessing

from modules.art import Art
from modules.scene_variants import \
    get_scene_variants, \
    scene_expected_exit_code_for_flags
from modules.report.html import HTMLReport
from modules.report.terminal import TerminalReport

import time

# Where to find different elements for the test & place the result
scene_dir     = 'scenes'
reference_dir = os.path.join('reference')
output_dir    = os.path.join('output', 'render')
report_dir    = os.path.join('output', 'report')

# This suite is made for a specific version of ART
art_version_major = 2
art_version_minor = 1
art_version_patch = 2
art_version_devtag = ''

class colour:
    PURPLE    = '\033[95m'
    CYAN      = '\033[96m'
    DARKCYAN  = '\033[36m'
    BLUE      = '\033[94m'
    GREEN     = '\033[92m'
    YELLOW    = '\033[93m'
    RED       = '\033[91m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'
    END       = '\033[0m'

ok_message_template    = '\t' + colour.BOLD + colour.GREEN + \
                         '[ OK  ]' + colour.END + ' {}'

err_message_template   = '\t' + colour.BOLD + colour.RED + \
                         '[ERROR]' + colour.END + ' {}'

warn_message_template  = '\t' + colour.BOLD + colour.YELLOW + \
                         '[WARN ]' + colour.END + ' {}'

title_message_template = 2*'\n' + colour.UNDERLINE + colour.BLUE \
                         + '{}' + colour.END

filename_template      = colour.CYAN + '{}' + colour.END

def empty_render():
    return {
        'cmd'         : '',
        'generated'   : False,
        'output'      : '',
        'output_raw'  : '',
        'output_noext': '',
        'output_ldr'  : '',
        'log'         : '',
        'stderr'      : '',
        'polarised'   : False,
        'dop_noext'   : '',
        's0_noext'    : '',
        's1_noext'    : '',
        's2_noext'    : '',
        's3_noext'    : '',
        'return_code' : -1,
        'success'     : False,
    }

def render(scene_file, output_path, args, expected_return_value):
    render_result = empty_render()

    sucess_render_msg  = ok_message_template.format (
        'Rendering was executed correctly:      ' + filename_template
    )
    failure_render_msg = err_message_template.format(
        'Rendering was not executed correctly:  ' + filename_template
    )
    
    stderr_log_file = output_path + '.stderr'

    # Rendering
    render_result['cmd'], render_result['return_code'] =  Art.artist(
        scene_file,
        output_path,
        8,
        args,
        stderr_log_file
    )
    
    # Check if an error code was raised and if that was expected
    if render_result['return_code'] == expected_return_value:
        render_result['success'] = True

    # Get the error log
    if os.path.exists(stderr_log_file):
        with open(stderr_log_file) as log_file:
            render_result['stderr'] = log_file.readlines()

    # If artist did not executed correctly, end of the story
    if render_result['return_code'] != 0:
        return render_result

    # Names for output
    render_result['output_noext'] = output_path
    render_result['output']       = output_path + '.artcsp'
    render_result['output_raw']   = output_path + '.artraw'
    render_result['output_ldr']   = output_path + '.tiff'

    # Get the output
    if os.path.exists(render_result['output_noext'] + '.log'):
        with open(render_result['output_noext'] + '.log') as log_file:
            render_result['log'] = log_file.readlines()

    # Check if everything went right
    #if not os.path.exists(render_result['output']):
    if not os.path.exists(render_result['output_raw']):
        print(failure_render_msg.format(render_result['output_raw']))
        return render_result
    else:
        Art.tonemap(render_result['output_raw'])

    print(sucess_render_msg.format(render_result['output']))
    render_result['generated'] = True

    # If polarisation, should also generate polvis
    if '-p' in args:
        render_result['polarised'] = True

        Art.polvis(render_result['output_noext'] + '.artraw',
                   render_result['output_noext'],
                   ['-dop'])
        
        render_result['dop_noext'] = render_result['output_noext'] \
                                     + '.dop.550nm'

        Art.polvis(render_result['output_noext'] + '.artraw',
                   render_result['output_noext'],
                   ['-stc'])

        render_result['s0_noext'] = render_result['output_noext'] + '.550nm.s0'
        render_result['s1_noext'] = render_result['output_noext'] + '.550nm.s1'
        render_result['s2_noext'] = render_result['output_noext'] + '.550nm.s2'
        render_result['s3_noext'] = render_result['output_noext'] + '.550nm.s3'

    return render_result

def empty_compare():
    return {
        'bugblatter_cmd'  : '',
        'diff_possible'   : False,
        'diff_generated'  : False,
        'diff_noext'      : '',
        'diff_ldr'        : '',
        'diff_value'      : -1,
    }

def compare(reference_path, render_path, output_path):
    compare_result = empty_compare()
    
    sucess_compare_msg  = ok_message_template.format(
        'Comparison was executed correctly:     ' + filename_template
    )
    failure_compare_msg = err_message_template.format(
        'Comparison was not executed correctly: ' + filename_template
    )
    warning_compare_msg = err_message_template.format(
        'Cannot find the reference:             ' + filename_template
    )
    
    compare_result['diff_noext'] = output_path
    compare_result['diff_ldr']   = output_path + '.tiff'

    # check if the reference exists
    reference_path_with_ext_csp = reference_path + '.artcsp'
    reference_path_with_ext_raw = reference_path + '.artraw'

    if not os.path.exists(reference_path_with_ext_csp):
        if not os.path.exists(reference_path_with_ext_raw):
            print(warning_compare_msg.format(reference_path_with_ext_raw))
            return compare_result
        else:
            Art.tonemap(reference_path_with_ext_raw)

    compare_result['diff_possible'] = True
    
    # Diff image
    compare_result['bugblatter_cmd'] = Art.bugblatter(reference_path + '.artcsp',
                                                      render_path    + '.artcsp',
                                                      compare_result['diff_noext'])
    
    # Check if everything went right
    if os.path.exists(compare_result['diff_ldr']):
        print(sucess_compare_msg.format(compare_result['diff_ldr']))
        compare_result['diff_generated'] = True
    else:
        print(failure_compare_msg.format(compare_result['diff_ldr']))
        return compare_result

    # Diff value
    Art.art_imagediff(reference_path + '.artcsp',
                      render_path    + '.artcsp',
                      compare_result['diff_noext'] + '.txt')

    with open(compare_result['diff_noext'] + '.txt') as f:
        compare_result['diff_value'] = float(f.readline())

    return compare_result

def run_test(test_iter, n_tests,
             scene_path, output_path,
             reference_path,
             render_args,
             expected_return_value):
    test_result = {
        'render' : {},
        'compare' : {},
    }
    
    # Format for messages
    start_test_msg_template = title_message_template.format(
        'Test {current}/{total}:') \
        + colour.CYAN                 + ' {scene_path} '  + colour.END + '->' \
        + colour.GREEN + colour.BOLD  + ' {output_path} ' + colour.END

    # Full naame for output
    output_path_with_ext = output_path + '.artcsp'
    
    print(start_test_msg_template.format(current = test_iter, total = n_tests,
                                         scene_path = scene_path,
                                         output_path = output_path_with_ext ))
    # run artist
    test_result['render'] = render(
        scene_path,
        output_path,
        render_args,
        expected_return_value
    )

    # Compute the difference
    if expected_return_value == 0:
        compare_path = output_path + '_ErrorDiff'
        test_result['compare'] = compare(
            reference_path,
            output_path,
            compare_path
        )
    else:
        test_result['compare'] = empty_compare()

    return test_result

def get_joblist():
    # Holds a list of tuple with (scene_path, output_path, reference_path)
    job_list_temp = [ (s,
                       os.path.join(scene_dir, s),
                       os.path.join(output_dir, s[:-4]),
                       os.path.join(reference_dir, s[:-4]))
                       for s in os.listdir(scene_dir)
                       if s.endswith('.arm') and not s.startswith('.')
    ]


    # First, get the number of scenes to test
    n_jobs = 0
    n_comparisons = 0
    job_list = []

    print(title_message_template.format('Checking scenes to render:'))

    for (scene, scene_path, output_path, reference_path) in job_list_temp:
        # Check if arm -> art translation is possible
        print('\n\t' + colour.UNDERLINE + 'Checking:' + colour.END + \
              ' ' + colour.CYAN + scene + colour.END + '...'
        )

        scene_variants = get_scene_variants(scene)
        all_conversion_success = True
        
        for variant_name, variant_args in scene_variants:
            if Art.arm2art(scene_path, variant_args) != 0:
                all_conversion_success = False
                print('\t\t' + variant_name + ' failed to compile.')
                print(variant_args)
        if not all_conversion_success:
            print(err_message_template.format(
                'Tests will be ignored: some conversions failed for scene: '
                + scene)
            )
        else:
            print(ok_message_template.format(
                'Using scene: ' + scene)
            )
            n_jobs += 2 * len(scene_variants)
            job_list += [(scene, scene_path, output_path, reference_path)]
    
    return job_list, n_jobs

def generated_blessed():
    # Format for messages
    start_render_msg_template = title_message_template.format(
        'Render {current}/{total}:') \
        + colour.CYAN                 + ' {scene_path} '  + colour.END + '->' \
        + colour.GREEN + colour.BOLD  + ' {output_path} ' + colour.END

    
    # Delete the reference dir and recreate it if needed
    if os.path.exists(reference_dir):
        shutil.rmtree(reference_dir)
        
    os.makedirs(reference_dir)

    # Get the jobs that need to be done
    job_list, n_jobs = get_joblist()

    curr_job = 0

    #def process(j, n_jobs):
    #    (scene, scene_path, output_path, reference_path) = j
    for (scene, scene_path, output_path, reference_path) in job_list:
        scene_variants = get_scene_variants(scene)

        for variant_name, variant_args in scene_variants:
            for polarisation_name, polarisation_args in zip(['.1', '.2.pol'],
                                                            [[''], ['-p']]):
                suffix = variant_name + polarisation_name
                render_args = variant_args + polarisation_args
                
                expected_return_value = scene_expected_exit_code_for_flags(
                    scene,
                    render_args
                )
                
                curr_job += 1

                print(start_render_msg_template.format(current = curr_job, total = n_jobs,
                                                       scene_path = scene_path,
                                                       output_path = reference_path + suffix ))
                if expected_return_value == 0:
                    # run artist
                    render_result = render(
                        scene_path,
                        reference_path + suffix,
                        render_args,
                        expected_return_value
                    )

                    # Add to git
                    call(['git', 'add', render_result['output_raw']])

    #num_cores = multiprocessing.cpu_count()
    #results = Parallel(n_jobs=num_cores)(delayed(process)(j, n_jobs) for j in job_list)

    # Clean up extra files generated
    for f in os.listdir(reference_dir):
        if not f.endswith('.artraw'):
            os.remove(os.path.join(reference_dir, f))
        

                
def main():
    # Creates output dir if not existing
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        

    # Get the jobs that need to be done
    job_list, n_jobs = get_joblist()
        
    # Now, perform the tests
    curr_job = 0

    report = {
        'scenes': {},
        'tests_succeeded'     : [],
        'tests_failed'        : [],
        'renderings_succeeded': [],
        'renderings_failed'   : [],
        'compares_succeeded'  : [],
        'compares_failed'     : [],
        'compare_metric'      : [],
    }

    start = time.time()
    
    for (scene, scene_path, output_path, reference_path) in job_list:
        report['scenes'][scene] = []
        scene_variants = get_scene_variants(scene)
        
        for variant_name, variant_args in scene_variants:
            for polarisation_name, polarisation_args in zip(['.1', '.2.pol'],
                                                            [[''], ['-p']]):
                suffix = variant_name + polarisation_name
                args = variant_args + polarisation_args
                
                expected_return_value = scene_expected_exit_code_for_flags(
                    scene,
                    args
                )
                
                curr_job += 1
                test_result = run_test(curr_job, n_jobs,
                                       scene_path, output_path + suffix,
                                       reference_path + suffix,
                                       args,
                                       expected_return_value)

                scene_variant_name = scene[:-4] + suffix

                report['scenes'][scene].append({
                    'name'           : scene_variant_name,
                    'reference_path' : reference_path + suffix,
                    'test_res'       : test_result
                })

                if expected_return_value == 0:
                    if test_result['render']['generated']:
                        report['renderings_succeeded'].append(scene_variant_name)
                    else:
                        report['renderings_failed'].append(scene_variant_name)

                    if test_result['compare']['diff_possible']:
                        report['compares_succeeded'].append(scene_variant_name)
                    else:
                        report['compares_failed'].append(scene_variant_name)

                # A test is a success only if everything possible was
                # generated: If the rendering was possible, the comparison
                # should be possible and done. Also, have a look to the
                # comparison metric to see if there is a perfect match between
                # the reference and the rendering.
                # Otherwise, check if the error code is consistant with
                # what was expected

                # rendering + comparison was done
                if expected_return_value == 0 \
                   and test_result['render']['success'] \
                   and test_result['render']['generated'] \
                   and test_result['compare']['diff_possible']:
                    report['tests_succeeded'].append(scene_variant_name)
                    # comparison mismatch -> warning
                    if test_result['compare']['diff_value'] > 1e-4:
                        print(warn_message_template.format(
                            colour.YELLOW + 'Test passed but difference was found with reference: '
                            + str(test_result['compare']['diff_value'])))
                    # comparison match -> test passed
                    else:
                        print(ok_message_template.format(colour.GREEN + 'Test passed' + colour.END))
                # artist fails as expected -> test passed
                elif expected_return_value != 0 \
                     and test_result['render']['success']:
                    report['tests_succeeded'].append(scene_variant_name)
                    print(ok_message_template.format(colour.GREEN + 'Test passed' + colour.END))
                    # any other situation -> test failed
                else:
                    report['tests_failed'].append(scene_variant_name)
                    print(err_message_template.format(colour.RED + 'Test failed' + colour.END))


                # Save the compare diff only if it was performed
                if expected_return_value == 0 \
                   and test_result['compare']['diff_value'] > -1:
                    report['compare_metric'].append((scene_variant_name,
                                                     test_result['compare']['diff_value'])
                    )
                    
    end = time.time()
    report['time'] = round(end-start,3)
    
    print(title_message_template.format('Creating reports:'))
    
    print('\tBuilding html output...')
    HTMLReport.write(report, report_dir)
    print('\tBuilding console output...')
    TerminalReport.write(report, report_dir)

    print('\nReport(s) written in: ' + colour.GREEN + colour.BOLD + report_dir + colour.END)
    
if __name__ == '__main__':
    # Welcome message
    print('')
    print(colour.BOLD + 'art_functionality_test.py' + colour.END + \
            ' - Testing suite for ART')
    print('ART version ' + Art.version_str(
        art_version_major,
        art_version_minor,
        art_version_patch,
        art_version_devtag)
        )
    print('(c) 1996-2018 by the ART development team')
    

    # Checking if we are using the current version
    print(title_message_template.format('Checking if ART toolkit version matches:'))

    vers_match = Art.check_version_match(
        art_version_major,
        art_version_minor,
        art_version_patch,
        art_version_devtag
    )
    
    if vers_match:
        print(ok_message_template.format('Functionality test version matches ART version'))
    else:
        print(warn_message_template.format('Functionality test version does not match ART version'))

    # Now, check which mode we are running
    if len(sys.argv) < 2:
        main()
    elif sys.argv[1] == '--blessed':
        print(title_message_template.format('Blessed mode'))
        print(warn_message_template.format( colour.YELLOW +
            'This mode is going to replace the references images by regenerating those using'
        ))
        print(warn_message_template.format( colour.YELLOW +
            'the current installed ART toolkit to generate those. Make sure it is what you'
        ))
        print(warn_message_template.format( colour.YELLOW +
            'want: generated images will be then considered as references.' + colour.END
        ))

        answer = input(colour.BOLD + '\tProceed ? [y/N]' + colour.END)
        if answer == 'y' or answer == 'Y':
            print('\tOk, if that\'s your choice... Proceeding')
            generated_blessed()
        else:
            print('\tAborded')
    else:
        print()
        print('Usage: ./art_functionality_test.py [--blessed]')
        print('\t--blessed option replaces the current references by regenerating them.')
        print('\tIf no option is specified, the tests are performed.')
