set terminal pngcairo size 1920,920 font "Times,10"
set output filename_out

set xtics nomirror rotate by -90
set style data histogram
set style histogram cluster gap 1

plot filename_in u 2:xtic(1) noti

set output